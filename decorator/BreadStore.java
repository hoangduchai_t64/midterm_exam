package com.decorator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class BreadStore {
	private List<Bread> breads;

	public BreadStore() {
		breads = new LinkedList<>();
	}

	/**
	 * Giả sử bánh mỳ được làm không cho một loại gia vị nhiều hơn một lần.
	 * Bắt đầu, tạo ra và cho vào cửa hàng:
	 *  5 bánh mỳ ThickcrustBread chỉ có cheese,
	 *  5 bánh mỳ ThickcrustBread chỉ có olives,
	 *  5 bánh mỳ ThickcrustBread có cả cheese và olives,
	 *  5 bánh mỳ ThincrustBread chỉ có cheese,
	 *  5 bánh mỳ ThincrustBread chỉ có olives,
	 *  5 bánh mỳ ThincrustBread có cả cheese và olives.
	 */
	public void init() {
//		ThincrustBread thincrustBread1 = new ThincrustBread();
//		ThincrustBread thincrustBread2 = new ThincrustBread();
//		ThincrustBread thincrustBread3 = new ThincrustBread();
//		Olives olives = new Olives(thincrustBread1);
//		Cheese cheese = new Cheese(thincrustBread1);
		for (int i = 1; i <= 1; i++) {
			breads.add(new Olives(new ThincrustBread()));
			breads.add(new Cheese(new ThincrustBread()));
			breads.add(new Olives(new Cheese(new ThincrustBread())));
			breads.add(new Olives(new ThickcrustBread()));
			breads.add(new Cheese(new ThickcrustBread()));
			breads.add(new Olives(new Cheese(new ThickcrustBread())));

		}

	}

	/**
	 * Thêm bánh mỳ vào cửa hàng.
	 */
	public void add(Bread bread) {
		breads.add(bread);
	}

	/**
	 * Giả sử cửa hàng bán một cái bánh mỳ nào đó,
	 * Bánh mỳ được lấy ra để bán là bánh mỳ đầu tiên có giá bằng giá
	 *  bánh mỳ yêu cầu.
	 * Nếu còn bánh mỳ để bán thì trả về giá trị true,
	 *  nếu không còn trả về giá trị false.
	 */
	public boolean sell(Bread bread) {
		for (Bread b: breads) {
			if( bread.equals(b) && bread.cost() == b.cost()){
				breads.remove(b);
				return true;
			}
		}
		return false;
	}


	/**
	 * In ra những bánh mỳ còn trong cửa hàng.
	 */
	public void print() {
		if (breads == null){
			return;
		}
		for (Bread b:
			 breads) {
			System.out.println(b.getDescription() + "  " +b.cost());
		}
	}

	/**
	 * Sắp xếp các bánh mỳ còn lại theo thứ tự được cho bởi order,
	 * nếu order là true, sắp xếp theo thứ tự tăng dần,
	 * nếu order là false, sắp xếp theo thứ tự giảm dần.
	 * Việc sắp xếp không làm thay đổi thứ tự của bánh mỳ trong cửa hàng.
	 */
	public List<Bread> sort(boolean order) {
		for (int i = 0; i < breads.size()-1; i++) {
			Bread bread = breads.get(i);
			for (int j = i+1; j < breads.size(); j++) {
				if(order){
					if(bread.cost() > breads.get(j).cost()){
						bread = breads.get(j);
						breads.set(j,breads.get(i));
						breads.set(i,bread);
					}
				}else {
					if(bread.cost() < breads.get(j).cost()){
						bread = breads.get(j);
						breads.set(j,breads.get(i));
						breads.set(i,bread);
					}
				}


			}

		}
	return breads;
	}

	/**
	 * Lọc ra howMany cái bánh mỳ có giá cao nhất hoặc thấp nhất,
	 * nếu order là true thì lọc ra bánh mỳ có giá cao nhất,
	 * nếu order là false thì lọc ra bánh mỳ có giá thấp nhất.
	 */
	public List<Bread> filter(int howMany, boolean order) {
		if (howMany > breads.size()){
			return null;
		}
		List<Bread> breads1 = new ArrayList<>();
		sort(order);
		for (int i = 0; i < howMany; i++) {
			breads1.add(breads.get(i));
		}
		return breads1;
	}

	public static void main(String args[]) {
		BreadStore breadStore = new BreadStore();
		breadStore.init();

		/*
		* Sau khi khởi tạo số bánh mỳ cho cửa hàng, viết chương trình demo:
		* - Thêm một số bánh mỳ vào cửa hàng
		* - Bán một số bánh mỳ từ cửa hàng
		* - In ra số bánh mỳ còn lại theo thứ tự giá tăng dần.
		* - In ra nhiều nhất 10 cái bánh mỳ có giá thấp nhất còn trong cửa hàng.
		*/
//		breadStore.sort(false);
		breadStore.sell(new Olives(new ThincrustBread()));
//		breadStore.breads = breadStore.filter(10,true);
		breadStore.print();

	}
}
