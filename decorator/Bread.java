package com.decorator;

import java.util.Objects;

public abstract class Bread {
	String description = "Bread";
  
	public String getDescription() {
		return description;
	}
 
	public abstract double cost();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Bread bread = (Bread) o;
		return cost() == bread.cost();
	}

	@Override
	public int hashCode() {
		return Objects.hash(cost());
	}
}
