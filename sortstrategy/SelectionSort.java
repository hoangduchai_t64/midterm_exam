package com.sortstrategy;

public class SelectionSort implements ISort {
    @Override
    public int sort(int[] data) {
        int count = 0;
        for (int i = 0; i < data.length-1; i++) {
            int min = data[i];
            for (int j = i+1; j < data.length ; j++) {
                if(min > data[j]){
                    min = data[j];
                    count++;
                }
            }
            data[i] = min;
        }
        return count;
    }
}
