package com;

public class MyMath {
    public static  double cos(double x){
        double err = 1e-12;
        double cos = 1;
        double phanso = 1;
        int i = 2;
        while (Math.abs(phanso) > err){
            phanso *= -1*x*x / i / (i-1);
            cos += phanso;
            i += 2;
        }
        return cos;
    }
    public static  double exp(double x){
        double err = 1e-12;
        double exp = 1;
        double phanso = 1;
        int i = 1;
        while (Math.abs(phanso) > err) {
            phanso *= x / i;
            exp += phanso;
            i++;
        }
        return exp;
    }
    public static  String toDecimal(String input, int radix){
        input = input.toUpperCase();
        int result = input.charAt(0) - 'A' + 10;
        if(input.charAt(0) >= '0' && input.charAt(0) <= '9'){
            result  = Integer.parseInt(input.charAt(0)+"");
        }else if(input.charAt(0) >= 'A' && input.charAt(0) <= 'F'){
            result = input.charAt(0) - 'A' + 10;
        }
        for (int i = 1; i < input.length(); i++) {
            char c = input.charAt(i);
            int temp = 0;
            if(c >= '0' && c <= '9'){
                temp = Integer.parseInt(c+"");
            }else if(c >= 'A' && c <= 'F'){
                temp = c - 'A' + 10;
            }
            result = result*radix + temp;
        }
        return result+"";
    }

    public static String toRadix(String number, int inRadix, int outRadix){
        if(inRadix == 10){
            return decimalTo(number,outRadix);
        }
        if(inRadix == outRadix){
            return number;
        }
        String dec = toDecimal(number, inRadix);
        System.out.println(dec);
        return decimalTo(dec,outRadix);
    }
    public static String decimalTo(String input, int outRadix){
        input = input.toUpperCase();
        int number = Integer.parseInt(input);
        StringBuilder result = new StringBuilder("");
        while (number > 0){
            int temp = number % outRadix;
            String ascii = "";
            if(temp >= 10 && temp <= 15){
                ascii = Character.toString(temp - 10 + 'A');
                result.append(ascii);
            }else {
                result.append(temp);
            }
            number /= outRadix;
        }
        return result.reverse().toString();
    }

    public static void main(String[] args) {
        String result = "e^(2.2) * cos(3.3) = " + exp(2.2) * cos(3.3) + "\n";
//        result += "using java libraries: " + Math.exp(2.2) * Math.cos(3.3) + "\n\n";
//        System.out.println(result);
//        System.out.println(toDecimal("6A1F",16));
//        System.out.println(decimalTo("12",2));
        System.out.println(toRadix("6A1F",16,8));
    }
}
