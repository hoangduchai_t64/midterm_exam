package com.iterator;

import java.util.Scanner;

public class MyArray implements MyIterable {
	private static final int MAX_ITEMS = 6;
	private int numberOfItems;
	private String[] menuItems;

	public MyArray() {
		numberOfItems = 0;
		menuItems = new String[MAX_ITEMS];
	}

	public void addItem(String name) {
        if(numberOfItems >= menuItems.length){
            String[] temp = new String[numberOfItems + 16];
            for (int i = 0; i < menuItems.length; i++) {
                temp[i] = menuItems[i];
            }
            menuItems = temp;
        }
        menuItems[numberOfItems++] = name;

	}

	public String[] getMenuItems() {
        return  menuItems;
	}

	public Iterator createIterator() {
        Iterator iterator = new Iterator() {
            String[] temp = menuItems;
            int count = 0;
            @Override
            public boolean hasNext() {
                return count < temp.length;
            }
            @Override
            public Object next() {
                return temp[count++];
            }
        };
        return iterator;
	}

	public String toString() {
        String result = "[ ";
        Iterator iterator = createIterator();
        while (iterator.hasNext()){
            result += iterator.next() + ", ";
        }
        result += "]";

		return result;
	}
}
