package com.iterator;

import java.util.ArrayList;

public class MyList implements MyIterable {
	private ArrayList<String> menuItems;

	public MyList() {
		menuItems = new ArrayList<String>();
	}

	public void addItem(String name) {
        menuItems.add(name);
	}

	public ArrayList<String> getMenuItems() {
        return  menuItems;
	}

	public Iterator createIterator() {
        Iterator iterator = new Iterator() {
            ArrayList<String > list = menuItems;
			int position = 0;
            @Override
            public boolean hasNext() {
                return position < list.size();
            }

            @Override
            public Object next() {
                return list.get(position++);
            }
        };
        return iterator;
	}

	public String toString() {
		return  menuItems.toString();
	}
}
